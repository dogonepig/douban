// 1,引用vue和路由
import Vue from "vue"
import Router from "vue-router"
// 4,将路由注入到vue中（让vue中能使用路由功能）
Vue.use(Router)
// 3,创建路由对象和规则
export default new Router({
    routes:[
        // 路由懒加载
        {name:"all",path:"/xiangqing",component:()=>import("@/pages/all/all.vue")},
        {name:"guangbo",path:"/guangbo",component:()=>import("@/pages/guangbo/guangbo.vue")},
        {name:"home",path:"/home",component:()=>import("@/pages/home/home.vue")},
        {name:"login",path:"/login",component:()=>import("@/pages/login/login.vue")},
        {name:"movie",path:"/movie",component:()=>import("@/pages/movie/movie.vue")},
        {name:"search",path:"/search",component:()=>import("@/pages/search/search.vue")},
        {name:"xiaozu",path:"/xiaozu",component:()=>import("@/pages/xiaozu/xiaozu.vue")},
        {name:"zhuce",path:"/zhuce",component:()=>import("@/pages/zhuce/zhuce.vue")},
        {name:"book",path:"/book",component:()=>import("@/pages/book/book.vue")},
        {path:"*",redirect:"/home"}
    ]
})
    



