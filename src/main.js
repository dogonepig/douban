import Vue from 'vue'
import App from './App.vue'
// 引用mock
require("./mock/index")

Vue.config.productionTip = false
import router from "@/router/index.js"

import axios from "axios"
Vue.prototype.axios=axios
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
